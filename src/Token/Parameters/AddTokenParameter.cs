﻿namespace Coscine.Api.Token.Parameters
{
    /// <summary>
    /// Used to parse the json request for add token.
    /// </summary>
    public class AddApiTokenParameter
    {
        /// <summary>
        /// Gets or sets the name of the token. 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the expiration of the token. 
        /// </summary>
        public uint? Expiration { get; set; }
    }
}
