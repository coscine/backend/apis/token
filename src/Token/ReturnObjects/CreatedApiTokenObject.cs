﻿using Coscine.Database.ReturnObjects;

namespace Coscine.Api.Token.ReturnObjects
{
     /// <summary>
     /// Retuned upon creation of a new token.
     /// Contains the jwt token.
     /// </summary>
    public class CreatedApiTokenObject : ApiTokenObject
    {
        /// <summary>
        /// Gets or sets the jwt token value of the token. 
        /// </summary>
        public string Token { get; set; }
    }
}
