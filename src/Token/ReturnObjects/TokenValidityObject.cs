﻿namespace Coscine.Api.Token.ReturnObjects
{
    /// <summary>
    /// Contains the minimum and maximum token validity in days from today.
    /// </summary>
    public class TokenValidityObject
    {
        /// <summary>
        /// Object holding the specified token minumum and maximum validity in days from today.
        /// </summary>
        public uint MinDays { get; set; }
        public uint MaxDays { get; set; }
    }
}
