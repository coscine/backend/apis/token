﻿using Coscine.Api.Token.Parameters;
using Coscine.Api.Token.ReturnObjects;
using Coscine.ApiCommons;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.JwtHandler;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Token.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a user object.
    /// </summary>

    [Authorize]
    public class TokenController : Controller
    {
        private readonly ApiTokenModel _apiTokenModel;
        private readonly Authenticator _authenticator;
        private readonly JWTHandler _jwtHandler;
        private readonly uint _maxDays;
        private readonly uint _minDays;
        private readonly uint _defaultExpirationDays;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public TokenController()
        {
            _apiTokenModel = new ApiTokenModel();
            _authenticator = new Authenticator(this, Program.Configuration);
            _jwtHandler = new JWTHandler(Program.Configuration);
            _maxDays = 370;
            _minDays = 7;
            _defaultExpirationDays = 180;
        }

        /// <summary>
        /// List all tokens of the current user.
        /// </summary>
        /// <returns>200 (Json) of list of tokens, 401 if unauthorized.</returns>
        [HttpGet("[controller]")]
        public ActionResult<IEnumerable<ApiTokenObject>> GetUserTokens()
        {
            var user = _authenticator.GetUserId();
            var tokens = _apiTokenModel.GetTokens(user);
            if (tokens == null)
            {
                return Json(new List<ApiTokenObject>());
            }

            return Json(tokens);
        }

        /// <summary>
        /// Get the specific token values.
        /// </summary>
        /// <param name="tokenId">User tokenId</param>
        /// <returns>StatusCode 200 (json) if ok, 404 if not found, 401 if unauthorized.</returns>
        [HttpGet("[controller]/{tokenId}")]
        public ActionResult<ApiTokenObject> GetUserToken(Guid tokenId)
        {
            var t = _apiTokenModel.GetToken(_authenticator.GetUser().Id, tokenId);
            if (t == null)
            {
                return NotFound("Token not found");
            }

            return Json(t);
        }

        /// <summary>
        /// Revoke a specific token.
        /// </summary>
        /// <param name="tokenId">User tokenId</param>
        /// <returns>StatusCode 204 successful revokation, 404 if token is not found, 401 if unauthorized.</returns>
        [HttpDelete("[controller]/{tokenId}")]
        public IActionResult RevokeToken(Guid tokenId)
        {
            var r = _apiTokenModel.Revoke(_authenticator.GetUser().Id, tokenId);
            if (r == 0)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new token for the user.
        /// </summary>
        /// <param name="addApiTokenParameter">Parsed from json</param>
        /// <returns>StatusCode 200 (Json) if ok, 400 if a value of data is not set or invalid, 401 if unauthorized.</returns>
        [HttpPut("[controller]")]
        public ActionResult<CreatedApiTokenObject> AddToken([FromBody] AddApiTokenParameter addApiTokenParameter)
        {
            if (addApiTokenParameter.Name == null)
            {
                return BadRequest("Missing description.");
            }

            var expiration = _defaultExpirationDays;
            if (addApiTokenParameter.Expiration.HasValue)
            {
                expiration = addApiTokenParameter.Expiration.Value;
            }

            expiration = Math.Min(expiration, _maxDays);
            expiration = Math.Max(expiration, _minDays);

            var iat = DateTime.Now;
            var exp = new DateTime(iat.Year, iat.Month, iat.Day, 23, 59, 59).AddDays(expiration);
            var tokenId = Guid.NewGuid();

            var jwtToken = _jwtHandler.GenerateJwtToken(new Dictionary<string, string> { { "tokenId", tokenId.ToString() } }, "https://coscine.rwth-aachen.de", "https://coscine.rwth-aachen.de", iat, exp);

            var userId = _authenticator.GetUser().Id;

            _apiTokenModel.InsertToken(tokenId, iat, exp, userId, addApiTokenParameter.Name);
            var token = new CreatedApiTokenObject { Name = addApiTokenParameter.Name, Created = iat, Expires = exp, TokenId = tokenId, Token = jwtToken };
            return Json(token);
        }

        /// <summary>
        /// Get the specified token minumum and maximum validity in days.
        /// </summary>
        /// <returns>StatusCode 200 if ok.</returns>
        [HttpGet("[controller]/getValidity")]
        public ActionResult<TokenValidityObject> GetTokenValidityBounds()
        {
            return Ok(new TokenValidityObject()
            {
                MaxDays = _maxDays,
                MinDays = _minDays,
            });
        }
    }
}
