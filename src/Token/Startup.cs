﻿using Coscine.ApiCommons;

namespace Coscine.Api.Token
{
    /// <summary>
    /// Startup class using AbstractStartup.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Startup()
        {
        }
    }
}
